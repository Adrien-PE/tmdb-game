import React from 'react';
import './EnterLeaderboard.css';
import { addToLeaderboard, canEnterLeaderboard } from '../../../services/leaderboard';

class EnterLeaderboard extends React.Component {
  constructor() {
    super();
    this.state = { added: false, name: '' };
  }

    handleNickNameChange = (e) => {
      this.setState({ name: e.target.value });
    }

    handleEnterLeaderboard = () => {
      const { score } = this.props;
      const { name } = this.state;
      addToLeaderboard({ name, score });
      this.setState({ added: true });
    }

    render() {
      const { added } = this.state;
      const { score } = this.props;
      if (added) {
        return (
          <div className="enter-leaderboard-container">
            Your score has been added to the leaderboard
          </div>
        );
      }
      if (canEnterLeaderboard(score)) {
        return (
          <div className="enter-leaderboard-container">
            Congratulations! You're in the top scores
            <div>
              <label htmlFor="name">Your name:</label>
              <input name="name" onChange={this.handleNickNameChange} />
              <button onClick={this.handleEnterLeaderboard} className="nes-btn" type="button">Enter Leaderboard</button>
            </div>

          </div>
        );
      }
      return <div />;
    }
}
export default EnterLeaderboard;
