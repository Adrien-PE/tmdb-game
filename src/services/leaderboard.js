const LOCAL_STORAGE_KEY = 'leaderboard';
export const NB_LEADERBOARD_ENTRIES = 10;

export const getLeaderboard = () => {
  const leaderboard = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
  return leaderboard || [];
};

export const addToLeaderboard = (entry) => {
  const leaderboard = getLeaderboard();
  if (leaderboard.length === NB_LEADERBOARD_ENTRIES) {
    leaderboard.splice(-1, 1);
  }
  leaderboard.push(entry);
  leaderboard.sort((a, b) => b.score - a.score);
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(leaderboard));
};

export const canEnterLeaderboard = (score) => {
  const leaderboard = getLeaderboard();
  if (leaderboard.length < NB_LEADERBOARD_ENTRIES) return true;
  const lowestScore = leaderboard[leaderboard.length - 1].score;
  if (score > lowestScore) return true;
  return false;
};
