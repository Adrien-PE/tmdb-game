import React from 'react';
import './Menu.css';
import { Link } from 'react-router-dom';

const Menu = () => (
  <div className="menu-container">
    <Link to="/play">
      <i className="snes-logo" />
      Play
    </Link>
    <Link to="/highscores">
      <i className="nes-icon star" />
      Leaderboard
    </Link>
  </div>
);


export default Menu;
