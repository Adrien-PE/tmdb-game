import React, { Component } from 'react';
import './Play.css';
import Start from '../Start';
import Game from '../Game';
import Gameover from '../Gameover';
import { DatasetProvider } from '../../stores/dataset';
import { ScoreProvider } from '../../stores/score';

class Play extends Component {
  constructor() {
    super();
    this.state = { gameScreen: 'start' };
  }

  handleStart = () => {
    this.setState({ gameScreen: 'game' });
  }

  handleGameOver = () => {
    this.setState({ gameScreen: 'gameover' });
  }

  playAgain = () => {
    this.setState({ gameScreen: 'start' });
  }

  render() {
    const { gameScreen } = this.state;

    return (
      <DatasetProvider>
        <ScoreProvider>
          {gameScreen === 'start' && <Start handleStart={this.handleStart} />}
          {gameScreen === 'game' && <Game handleGameOver={this.handleGameOver} />}
          {gameScreen === 'gameover' && <Gameover playAgain={this.playAgain} />}
        </ScoreProvider>
      </DatasetProvider>
    );
  }
}

export default Play;
