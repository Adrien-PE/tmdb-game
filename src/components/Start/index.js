import React from 'react';
import './Start.css';
import DatasetLoader from '../../containers/DatasetLoader';
import { withDataset } from '../../stores/dataset';
import StartButton from './StartButton';

const Start = ({ dataset, setDataset, handleStart }) => {
  const isGameReady = !!dataset;
  return (
    <div className="nes-container with-title is-centered start-container">
      <p className="title">TMDB Game</p>
      <p>
        Test your cinema culture by saying if an actor has played in a specific movie.
        You have 30 seconds to give the maximum number of correct answers.
      </p>
      <DatasetLoader setDataset={setDataset} />
      <StartButton onClick={handleStart} enabled={isGameReady} />
    </div>
  );
};

export default withDataset(Start);
