import { shuffle } from 'lodash';

export default class QuestionGenerator {
  constructor(dataset) {
    this.trueQuestions = shuffle(dataset.trueQuestions);
    this.falseQuestions = shuffle(dataset.falseQuestions);
    this.trueIndex = 0;
    this.falseIndex = 0;
  }

  getNextQuestion = () => {
    const isNextQuestionTrue = this.isNextQuestionTrue();
    if (isNextQuestionTrue) {
      const question = this.trueQuestions[this.trueIndex];
      this.trueIndex += 1;
      return { question, answer: true };
    }
    const question = this.falseQuestions[this.falseIndex];
    this.falseIndex += 1;
    return { question, answer: false };
  }

  isNextQuestionTrue = () => Math.random() < 0.5
}
