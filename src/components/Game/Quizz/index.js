import React from 'react';
import './Quizz.css';
import TMDBImage from './TMDBImage';

const Quizz = ({
  actor, movie, handlePositiveAnswer, handleNegativeAnswer,
}) => (
  <div className="question-container">
    <div className="movie-actor-container">
      <div className="actor-container">
        <TMDBImage path={actor.path} type="actor" name={actor.name} />
        {actor.name}
      </div>
      <div className="movie-container">
        <TMDBImage path={movie.path} type="movie" name={movie.name} />
        {movie.name}
      </div>
    </div>
    <div className="answer-buttons-container">
      <button onClick={handlePositiveAnswer} className="nes-btn is-success" type="button">Yes</button>
      <button onClick={handleNegativeAnswer} className="nes-btn is-error" type="button">No</button>
    </div>
  </div>
);

export default Quizz;
