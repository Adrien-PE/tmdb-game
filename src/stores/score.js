import React, { Component } from 'react';

const ScoreContext = React.createContext();

export class ScoreProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      score: 0,
    };
  }

  setScore = (score) => {
    this.setState({ score });
  }

  render() {
    const { children } = this.props;
    const { score } = this.state;

    return (
      <ScoreContext.Provider
        value={{
          score,
          setScore: this.setScore,
        }}
      >
        {children}
      </ScoreContext.Provider>
    );
  }
}

export const withScore = Component => props => (
  <ScoreContext.Consumer>
    {({ score, setScore }) => <Component {...props} score={score} setScore={setScore} />}
  </ScoreContext.Consumer>
);
