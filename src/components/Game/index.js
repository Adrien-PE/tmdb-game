import React from 'react';
import { withDataset } from '../../stores/dataset';
import { withScore } from '../../stores/score';
import Timer from './Timer';
import Quizz from './Quizz';
import Score from './Score';
import QuestionGenerator from './QuestionGenerator';
import './Game.css';

class Game extends React.Component {
  constructor(props) {
    super();
    this.questionGenerator = new QuestionGenerator(props.dataset);
    const { question, answer } = this.questionGenerator.getNextQuestion();
    this.state = {
      currentQuestion: question,
      currentAnswer: answer,
    };
  }

  handlePositiveAnswer = () => {
    this.handleAnswer(true);
  }

  handleNegativeAnswer = () => {
    this.handleAnswer(false);
  }

  handleAnswer = (answer) => {
    const { handleGameOver, score, setScore } = this.props;
    const { currentAnswer } = this.state;
    if (answer === currentAnswer) {
      setScore(score + 1);
      this.setNextQuestion();
    } else {
      handleGameOver();
    }
  }

  handleTimerEnd = () => {
    const { handleGameOver } = this.props;
    handleGameOver();
  }

  setNextQuestion = () => {
    const { question, answer } = this.questionGenerator.getNextQuestion();
    this.setState({
      currentQuestion: question,
      currentAnswer: answer,
    });
  }

  render() {
    const { currentQuestion: { actor, movie } } = this.state;
    const { score } = this.props;
    return (
      <div className="game-container nes-container with-title is-centered">
        <p className="title">Quizz</p>
        <Timer handleTimerEnd={this.handleTimerEnd} />
        <Score score={score} />
        <Quizz
          actor={actor}
          movie={movie}
          handlePositiveAnswer={this.handlePositiveAnswer}
          handleNegativeAnswer={this.handleNegativeAnswer}
        />
      </div>
    );
  }
}

export default withDataset(withScore(Game));
