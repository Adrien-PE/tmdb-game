import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Play from './components/Play';
import Leaderboard from './components/Leaderboard';
import Menu from './components/Menu';
import './App.css';

const App = () => (
  <Router>

    <div className="app-container">
      <Menu />
      <Route exact path="/play" component={Play} />
      <Route exact path="/highscores" component={Leaderboard} />
    </div>
  </Router>
);

export default App;
