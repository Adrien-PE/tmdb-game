import React from 'react';
import './Score.css';

const Score = ({ score }) => (
  <div>
    {`Your score is ${score}`}
  </div>
);

export default Score;
