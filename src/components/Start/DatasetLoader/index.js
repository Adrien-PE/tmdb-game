import React from 'react';
import './DatasetLoader.css';

export default ({ loading, error, handleRetry }) => {
  if (loading) {
    return (
      <div>Loading data...</div>
    );
  } if (error) {
    return (
      <div>
        <div>Error while downloading the data...</div>
        <button onClick={handleRetry} type="button" className="nes-btn is-warning">Retry</button>
      </div>
    );
  }
  return (
    <div> The game is Ready !</div>
  );
};
