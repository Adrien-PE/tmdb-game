import { getQuestions } from './questions';

const actor1 = { name: 'Hailee Steinfeld', path: '/vKwqK9DIlvoQlodbRTONWSghLk4.jpg' };
const actor2 = { name: 'Asa Butterfield', path: '/r5Cmtq7kB0QD0BV4NtYOUSeUQfx.jpg' };
const movie1 = { name: 'True Grit', path: '/qc7vaF5fIeTllAykdP3wldlMyRh.jpg' };
const movie2 = { name: "Ender's Game", path: '/zHRNhCkrFEMoudPr0uics5WZe7j.jpg' };
const movie3 = { name: 'The Wolfman', path: '/igiSz8bGHE0BegZ8xvlokAQgAk3.jpg' };
const movie4 = { name: 'Son of Rambow', path: '/7rDyK1mCUFxbQcxFHj9c2xumFCp.jpg' };
const movie5 = { name: 'Movie without poster', path: null };

const mock = [
  {
    name: actor1.name,
    path: actor1.path,
    movies: [movie1, movie2],
  },
  {
    name: actor2.name,
    path: actor2.path,
    movies: [movie3, movie4, movie5],
  },
];

const expected = {
  trueQuestions: [
    { actor: actor1, movie: movie1 },
    { actor: actor1, movie: movie2 },
    { actor: actor2, movie: movie3 },
    { actor: actor2, movie: movie4 },
  ],
  falseQuestions: [
    { actor: actor2, movie: movie1 },
    { actor: actor2, movie: movie2 },
    { actor: actor1, movie: movie3 },
    { actor: actor1, movie: movie4 },
  ],
};
describe('Questions helper', () => {
  it('should return approprate true and false questions', () => {
    const result = getQuestions(mock);
    expect(new Set(result.trueQuestions)).toEqual(new Set(expected.trueQuestions));
    expect(new Set(result.falseQuestions)).toEqual(new Set(expected.falseQuestions));
  });
});
