import React from 'react';
import './Leaderboard.css';
import { getLeaderboard } from '../../services/leaderboard';

const Leaderboard = () => {
  const leaderboard = getLeaderboard();
  if (leaderboard.length === 0) {
    return 'No one played so far...';
  }
  return (
    <section className="nes-container is-centered with-title leaderboard-container">
      <h2 className="title">Leaderboard</h2>
      <div className="nes-table-responsive">
        <table className="table-container nes-table is-centered">
          <thead>
            <tr>
              <th>Name</th>
              <th>Score</th>
            </tr>
          </thead>
          <tbody>
            {leaderboard.map(({ name, score }) => (
              <tr key={name}>
                <td>{name}</td>
                <td>{score}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default Leaderboard;
