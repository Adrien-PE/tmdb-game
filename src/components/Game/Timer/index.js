import React from 'react';
import './Timer.css';

const GAME_TIME_SECONDS = 30;
const DISPLAY_REFRESH_PERIOD_MS = 100;

class Timer extends React.Component {
  constructor() {
    super();
    this.state = {
      timeRemaining: GAME_TIME_SECONDS,
    };
  }

  componentDidMount() {
    const { handleTimerEnd } = this.props;
    this.startTime = new Date();
    this.timer = setTimeout(handleTimerEnd, GAME_TIME_SECONDS * 1000);
    this.clock = setInterval(this.setTimeRemaining, DISPLAY_REFRESH_PERIOD_MS);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
    clearInterval(this.clock);
  }

  setTimeRemaining = () => {
    const now = new Date();
    const elapsed = (now - this.startTime) / 1000;
    const remaining = GAME_TIME_SECONDS - elapsed;
    this.setState({ timeRemaining: remaining });
  }

  render() {
    const { timeRemaining } = this.state;
    const timeToDisplay = Math.min(Math.floor(timeRemaining) + 1, GAME_TIME_SECONDS);
    let progressClass = 'is-success';
    if (timeRemaining < (2 / 3) * GAME_TIME_SECONDS) progressClass = 'is-warning';
    if (timeRemaining < (1 / 3) * GAME_TIME_SECONDS) progressClass = 'is-error';

    return (
      <div>
        <progress className={`nes-progress ${progressClass}`} value={timeRemaining.toString()} max={GAME_TIME_SECONDS.toString()} />
        {`${timeToDisplay}s remaining`}
      </div>
    );
  }
}

export default Timer;
