const API_KEY = process.env.REACT_APP_TMDB_API_KEY;
const BASE_URI = 'https://api.themoviedb.org/3/';

if (!API_KEY) {
  console.error('You must set REACT_APP_TMDB_API_KEY as an env variable');
}

const fetchTmdb = (uri, queryParams = '') => fetch(`${BASE_URI}${uri}?api_key=${API_KEY}&${queryParams}`)
  .then((response) => {
    if (!response.ok) {
      throw new Error();
    }
    return response.json();
  });

export const fetchPagePopularActors = async (pageIndex) => {
  try {
    const { results } = await fetchTmdb('person/popular', `page=${pageIndex}`);
    return results;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

export const fetchActorMovieCredits = async (actorId, actorName, actorPath) => {
  try {
    const { cast } = await fetchTmdb(`person/${actorId}/movie_credits`);
    return {
      name: actorName,
      path: actorPath,
      movies: cast.map(({ original_title: name, poster_path: path }) => ({ name, path })),
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
};
