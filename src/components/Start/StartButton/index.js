import React from 'react';

export default ({ onClick, enabled }) => (
  <button
    onClick={onClick}
    disabled={!enabled}
    className={`nes-btn${enabled ? '' : ' is-disabled'}`}
    type="button"
  >
    Play !
  </button>
);
