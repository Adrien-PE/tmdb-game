import {
  shuffle, uniqWith, isEqual, flatMap,
} from 'lodash';

const pickNameAndPath = ({ name, path }) => ({ name, path });

const getAllQuestions = (actorsWithCredits) => {
  const allActors = actorsWithCredits.map(pickNameAndPath);
  let allMovies = flatMap(actorsWithCredits, ({ movies }) => movies.map(pickNameAndPath));
  allMovies = uniqWith(allMovies, isEqual);
  return flatMap(allActors, actor => allMovies.map(movie => ({ actor, movie })));
};


const isQuestionInArray = ({ actor: { name: actorName }, movie: { name: movieName } }, array) => !!array.find(q => q.actor.name === actorName && q.movie.name === movieName); //eslint-disable-line

const questionHasImages = ({ actor: { path: actorPath }, movie: { path: moviePath } }) => !!actorPath && !!moviePath; //eslint-disable-line

const getFalseQuestions = (allQuestions, trueQuestions) => {
  const falseQuestions = [];
  let i = 0;
  const allQuestionsShuffled = shuffle(allQuestions); // As we limit the number of false questions
  // we need to shuffle them to avoid having only the first actors
  while (i < allQuestionsShuffled.length && falseQuestions.length < trueQuestions.length) {
    const q = allQuestionsShuffled[i];
    if (!isQuestionInArray(q, trueQuestions) && questionHasImages(q)) {
      falseQuestions.push(q);
    }
    i += 1;
  }
  return falseQuestions;
};

const getTrueQuestions = actorsWithCredits => flatMap(actorsWithCredits, (({ name: actorName, path: actorPath, movies }) => movies.map(({ name: movieName, path: moviePath }) => ( //eslint-disable-line
  {
    actor: {
      name: actorName,
      path: actorPath,
    },
    movie: {
      name: movieName,
      path: moviePath,
    },
  }
)))).filter(questionHasImages);

export const getQuestions = (actorsWithCredits) => {
  const allQuestions = getAllQuestions(actorsWithCredits);
  const trueQuestions = getTrueQuestions(actorsWithCredits);
  const falseQuestions = getFalseQuestions(allQuestions, trueQuestions);
  return { trueQuestions, falseQuestions };
};
