import React, { Component } from 'react';
import DatasetLoader from '../../components/Start/DatasetLoader';
import { fetchDataset } from '../../services/dataset';

class DatasetLoaderContainer extends Component {
  constructor(props) {
    super();
    props.setDataset(null);
    this.state = {
      loading: true,
      error: false,
    };
  }

  componentDidMount() {
    this.fetchDataset();
  }

    fetchDataset = async () => {
      const { setDataset } = this.props;
      try {
        const dataset = await fetchDataset();
        setDataset(dataset);
        this.setState({ loading: false, error: false });
      } catch (e) {
        this.setState({ loading: false, error: true });
      }
    }

    handleRetry = () => {
      this.setState({ loading: true, error: false });
      this.fetchDataset();
    }

    render() {
      const { loading, error } = this.state;
      return (
        <DatasetLoader loading={loading} error={error} handleRetry={this.handleRetry} />
      );
    }
}

export default DatasetLoaderContainer;
