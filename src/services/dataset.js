import { fetchPagePopularActors, fetchActorMovieCredits } from './tmdb';
import { getQuestions } from '../helpers/questions';

const NB_ACTORS = 20; // Warning: do not increase these numbers until
const PAGES_PER_BATCH = 1; // rate limiting rules have not been enforced on the client side
const LOCAL_STORAGE_KEY = 'dataset';

const fetchPopularActors = async () => {
  let popularActors = [];
  let batchIndex = 0;
  let gotEnoughActors = false;
  while (!gotEnoughActors && batchIndex < NB_ACTORS) {
    // The second condition in the while() is to prevent an infinite loop
    // in the unexpected case where we get empty arrays when fetching popular actors
    try {
      const actorsFromBatch = await fetchBatchPopularActors(batchIndex); // eslint-disable-line
      popularActors = popularActors.concat(actorsFromBatch);
      batchIndex += 1;
      if (popularActors.length >= NB_ACTORS) gotEnoughActors = true;
    } catch (e) {
      throw new Error(e);
    }
  }
  return popularActors;
};

const fetchBatchPopularActors = async (batchIndex) => {
  const promises = [];
  for (let i = 1; i <= PAGES_PER_BATCH; i += 1) {
    promises.push(fetchPagePopularActors(batchIndex * PAGES_PER_BATCH + i));
  }
  const pagesResults = await Promise.all(promises);
  return pagesResults.flat();
};

const fetchActorsMovieCredits = async (actors) => {
  const promises = [];
  actors.forEach(({ id, name, profile_path: path }) => {
    promises.push(fetchActorMovieCredits(id, name, path));
  });
  const results = await Promise.all(promises);
  return results.flat();
};

const getLocalDataset = () => JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));

export const fetchDataset = async () => {
  const localDataset = getLocalDataset();
  if (!localDataset) {
    const dataset = await fetchPopularActors()
      .then(fetchActorsMovieCredits)
      .then(getQuestions);

    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(dataset));
    return dataset;
  }
  return localDataset;
};
