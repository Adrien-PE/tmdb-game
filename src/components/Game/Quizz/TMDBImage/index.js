import React from 'react';
import './TMDBImage.css';

const BASE_URL = 'https://image.tmdb.org/t/p/';
const SIZE_ACTOR = 'w185';
const SIZE_MOVIE = 'w185';

const TMDBImage = ({ name, path, type }) => {
  const size = type === 'actor' ? SIZE_ACTOR : SIZE_MOVIE;
  return (
    <img src={`${BASE_URL}${size}${path}`} alt={name} />
  );
};

export default TMDBImage;
