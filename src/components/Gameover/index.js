import React from 'react';
import './Gameover.css';
import EnterLeaderboard from './EnterLeaderboard';
import { withScore } from '../../stores/score';

class Gameover extends React.Component {
    resetScoreAndPlayAgain = () => {
      const { playAgain, setScore } = this.props;
      setScore(0);
      playAgain();
    }

    render() {
      const { score } = this.props;
      return (
        <div className="gameover-container nes-container with-title is-centered">
          <p className="title">Gameover</p>
          <div>
            {`Your score is ${score}`}
          </div>
          <EnterLeaderboard score={score} />
          <button onClick={this.resetScoreAndPlayAgain} className="nes-btn" type="button">Play again</button>
        </div>
      );
    }
}
export default withScore(Gameover);
