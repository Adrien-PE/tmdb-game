# Description
TMDB game allows you to test your cinema culture. Each question will show you an actor and a movie. You'll just have to say if this actor played in this movie. You'll have 30 seconds to give the maximum number of correct answers. If you give a wrong answer, gameover!
# Live demo
http://217.182.252.140:2000/play
# About
This game has been developed as a code challenge. I spent 6 hours figuring out the UX, architecture, TMDB api and developing it.
The code is far from perfect and is now used as a basis of discussion around my skills/knowledge.
# Notes
## Get started
```npm install```  
```REACT_APP_TMDB_API_KEY=my_api_key npm start```  
**You must** set REACT_APP_TMDB_API_KEY as an env variable (in the cmd line with `npm start` or in a .env file) 
## Decisions
### Dataset download
I decided to download the dataset of questions once before the game can be started.  
This way we're sure the user has a smooth experience as each question would be loaded locally (excepted images)  
This would not be ideal with low bandwidth connections  
### Game mode
I implemented a 'survivor' mode with 30 seconds to play before gameover
### Leaderboard storage
The leaderboard is stored in the localStorage of the client.  
It was easier to implement but would not allow to share scores across different devices  
## Time spent
I spent roughly 5 hours developing the game + 1 hour adding styles

## Limitations
### Testing
No UI testing is implemented  
Only the file `helpers/questions` has tests (it contains data processing easy to mess up)
### Error handling
The granularity of the error handling is coarse.  
An error in the dataset downloading will be handled and the user can retry the operation
### Styling
A basic system of stylesheet import in each component has been adopted.  
It is OK because of the restricted number of components but would not scale nicely. Styled components or scoped css through dynamic naming would prevent future overlaps.  
The game has been developed for desktop and no particular care has been given to smaller devices  
Usually I start with styling first but not for this project because I wanted to be able to deliver something functional even if I did not have time to style the components  
### Number of actors
The TMDB api enforces some rate limiting rules.  
We have not implemented processes to ensure these rules are respected on the client side.  
For the moment you should avoid changing the parameters `NB_ACTORS` and `PAGES_PER_BATCH` in `src/services/dataset.js`
### API key security
If deployed on a server and accessible to other users, the TMDB api key would leak as the client makes direct requests to this api.  
A solution to prevent that would be to setup a proxy on the server for such requests