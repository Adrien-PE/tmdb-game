import React, { Component } from 'react';

const DatasetContext = React.createContext();

export class DatasetProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataset: null,
    };
  }

  setDataset = (dataset) => {
    this.setState({ dataset });
  }

  render() {
    const { children } = this.props;
    const { dataset } = this.state;

    return (
      <DatasetContext.Provider
        value={{
          dataset,
          setDataset: this.setDataset,
        }}
      >
        {children}
      </DatasetContext.Provider>
    );
  }
}

export const withDataset = Component => props => (
  <DatasetContext.Consumer>
    {({ dataset, setDataset }) => (
      <Component {...props} dataset={dataset} setDataset={setDataset} />
    )}
  </DatasetContext.Consumer>
);
